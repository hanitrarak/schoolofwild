<?php 
	$menus = get_Menu(); 
	$disciplines = get_Discipline();
	$evenements = get_Evenement();
	
	$getAccueil = get_Accueil();
	$urlAccueil = $getAccueil[0]['url'];
	$getApropos = get_Apropos();
	$urlApropos = $getApropos[0]['url'];
	$getContact = get_Contact();
	$urlContact = $getContact[0]['url'];
?>
<header id="header">
	<nav class="navbar navbar-fixed-top" role="banner">
		<div class="container">
			<div class="navbar-header" style="margin-top:-10px">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="index.html"><img src="images/logo-school-of-wild.png" class="img-responsive" width="110px" alt="logo de l'école School Of Wild" title="logo de l'école School Of Wild"></a>
			</div>
			
			  <div class="container-fluid" style="margin-top:-15px">
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				  <ul class="nav navbar-nav navbar-right" style="margin-bottom:-15px">
					<li><a href="<?php echo $urlAccueil ?>.html"><h4 style="color:white;">Accueil</h4></a></li>
					<li><a href="<?php echo $urlApropos ?>.html"><h4 style="color:white;">A propos</h4></a></li>
					<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><h4 style="color:white">Cours<span class="caret"></span></h4></a>
					  <ul class="dropdown-menu" role="menu" style="margin-top:-15px">
						<?php foreach($disciplines as $discipline) { ?>
							<li><a href="cours-<?php echo splitEspace($discipline['discipline']); ?>-<?php echo $discipline['idDiscipline'] ?>.html"><?php echo $discipline['discipline'] ?></a></li>
						<?php } ?>
					  </ul>
					</li>
					<!--<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><h4 style="color:white">Evénements<span class="caret"></span></h4></a>
					  <ul class="dropdown-menu" role="menu" style="margin-top:-15px">
						<?php foreach($evenements as $evenement) { ?>
							<li><a href="#"><?php echo $evenement['titre'] ?></a></li>
						<?php } ?>
					  </ul>
					</li>-->
					<li><a href="<?php echo $urlContact ?>.html"><h4 style="color:white">Contact</h4></a></li>
				  </ul>
				</div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			
			<!--<div class="collapse navbar-collapse navbar-right dropdown">
				<ul class="nav navbar-nav" >
					<li class="active"><a href="index.php"><h4 style="color:white">Accueil</h4></a></li>
					<li><a href="apropos.php"><h4 style="color:white">A propos</h4></a></li>
					<li class=""><a href="cours.php"><h4 style="color:white">Cours</h4></a>
					</li>
					<li><a href="evenement.php"><h4 style="color:white">Evènement</h4></a></li>
					<li><a href="contact.php"><h4 style="color:white">Contact</h4></a></li>
				</ul>
			</div>-->
		</div><!--/.container-->
	</nav><!--/nav-->
	
</header><!--/header-->