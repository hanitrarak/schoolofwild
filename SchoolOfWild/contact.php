<?php 
	require('fonctions.php'); 
	$contact = get_Contact();
	$title = $contact[0]['title'];
	$h1 = $contact[0]['h1'];
	$h2 = $contact[0]['h2'];
	$url = $contact[0]['url'];
	
	if (strtolower($_GET['url']) != $url ) {
		header('location:'.$url.'.html');
	}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="Description" content="<?php echo $h2 ?>">
    <title><?php echo $title ?></title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">      
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]--> 
	
  </head>
  <body class="homepage">   
	<?php include('header.php'); ?>
		
	<div class="map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3773.919859328752!2d47.52859021442655!3d-18.93494108716899!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x21f07dd703b18b19%3A0x6a7ed0410de0291b!2sMille+et+une+etoile!5e0!3m2!1sfr!2smg!4v1522759128462">
		</iframe>
	</div>
	
	<section id="contact-page">
        <div class="container">
            <div class="center">        
                <h1 style="color:black"><?php echo $h1 ?></h1>
                <h2 class="lead" style="font-size:20px"><?php echo $h2 ?></h2>
            </div> 
            <div class="row contact-wrap" style=""> 
                <div class="status alert alert-success" style="display: none"></div>
                <form class="contact-form" name="contact-form" method="post" action="sendemail.php">
				<div class="row">
					<div class="col-md-2">
					</div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Nom *</label>
                            <input type="text" name="nom" class="form-control" required="required">
                        </div> 
                    </div>
                    <div class="col-md-4">
						<div class="form-group">
                            <label>Email *</label>
                            <input type="email" name="email" class="form-control" required="required">
                        </div>
                    </div>
					<div class="col-md-2">
					</div>
				</div>
				<div class="row">
					<div class="col-md-2">
					</div>
					<div class="col-md-8">
                        <div class="form-group">
                            <label>Message *</label>
                            <textarea name="message" required="required" class="form-control" rows="8"></textarea>
                        </div>                        
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Submit Message</button>
                        </div>
					</div>
					<div class="col-md-2">
					</div>
				</div>
                </form> 
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#contact-page-->

    <?php include('footer.php'); ?>
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>   
    <script src="js/wow.min.js"></script>
	<script src="js/main.js"></script>
  </body>
</html>