create database school;

create table accueil
(
	title varchar(100),
	h1 varchar(100),
	h2 varchar(100),
	h3 varchar(300),
	url varchar(100)
);
insert into accueil values ('School Of Wild - Ecole de danse urbaine','SCHOOL OF WILD </br> ecole de danse urbaine','Les �v�nements organis�s par School Of Wild','Les billets des �v�nements sont d�j� en vente aupr�s des �l�ves de <strong> School Of Wild </strong> et au Mille et une �toiles Tsimbazaza','accueil-school-of-wild-ecole-de-danse-urbaine');

create table apropos
(
	title varchar(100),
	h1 varchar(100),
	h21 varchar(100),
	h31 varchar(800),
	h22 varchar(100),
	h32 varchar(800),
	url varchar(100)
);
insert into apropos values ('Pr�sentation de l''�cole School Of Wild - School Of Wild','PRESENTATION DE L''ECOLE SCHOOL OF WILD','Historique de l''�cole','<strong> School of wild </strong>, cr�e en 2011, est la premi�re �cole de danse purement urbaine � Madagascar. Men� par <strong> Andriamahery Fr�d�rico </strong> du groupe <strong> GB Wild </strong>, elle ne se limite pas seulement � l''apprentissage de la danse avec des cours collectifs ou particuliers mais � un partage total dans le but de faire conna�tre un peu plus la danse au grand public gr�ce � differents concept d''�v�nements. Depuis 2013 elle a r�ussi � se placer en tant que pionnier dans ce milieu avec son premier �v�nement <strong> � Let''s Dance � </strong> qui s''est vu devenir �v�nement national puis international depuis 2017; avec la collaboration de � Dancehall Master World � ; � High Dih � qui a concr�tis� sa 2�me �dition en fin d�cembre 2017 et d''autres � venir...','Quelques photos de la School Of Wild','Plus qu''une �cole de danse, <strong> School Of Wild </strong> propose diff�rents types de services pour des �v�nements de tout genre. Diverses animations de mariage, anniversaire, des After work pour les entreprises, etc ... </br> Des spectacles, des shows mais aussi des tournages publicitaires. </br> Elle a d�j� travaill� avec diff�rentes entreprises telles que <strong> Orange </strong>,<strong> Vero Vero Hair </strong>, <strong> Galax </strong>, et en collaboration �troite avec diff�rents m�dias comme <strong> Dream''in </strong>, <strong> Record </strong> et <strong> Viva </strong>.','apropos-presentation-de-l-ecole-school-of-wild');

create table contact 
(
	title varchar(100),
	h1 varchar(100),
	h2 varchar(200),
	url varchar(100)
);
insert into contact values ("Contact - School Of Wild", "Contacter School Of Wild","Contactez-nous pour plus d'information ou si vous avez des questions.","contact-contacter-school-of-wild");

/*create table presentation
(
	contenu varchar(800)
);
insert into presentation values ('School of wild, cr�e en 2011, est la premi�re �cole de danse purement urbaine � Madagascar. Men� par Andriamahery Fr�d�rico du groupe GB Wild, elle ne se limite pas seulement � l''apprentissage de la danse avec des cours collectifs ou particuliers mais � un partage total dans le but de faire conna�tre un peu plus la danse au grand public gr�ce � differents concept d''�v�nements. Depuis 2013 elle a r�ussi � se placer en tant que pionnier dans ce milieu avec son premier �v�nement � Let''s Dance � qui s''est vu devenir �v�nement national puis international depuis 2017; avec la collaboration de � Dancehall Master World � ; � High Dih � qui a concr�tis� sa 2�me �dition en fin d�cembre 2017 et d''autres � venir...');*/


/*create table menu
(
	id int not null auto_increment primary key,
    nomMenu varchar(20),
	url varchar(20),
	page varchar(20)
);
insert into menu values ('','Accueil','accueil-school-of-wild','index.php');
insert into menu values ('','A propos','a-propos-school-of-wild','apropos.php');
insert into menu values ('','Cours','cours-school-of-wild','cours.php');
insert into menu values ('','Ev�nement','evenement-school-of-wild','evenement.php');
insert into menu values ('','Contact','contact-school-of-wild','contact.php');*/

create table prof
(
	id int not null auto_increment primary key,
    prenom varchar(20)
);
insert into prof values ('','Teetoon');
insert into prof values ('','Brenda');
insert into prof values ('','Hanitra');

create table discipline
(
	idDiscipline int not null auto_increment primary key,
    idProf int,
	title varchar(100),
	discipline varchar(20),
	sousTitre varchar(300),
	photo varchar(100),
	alt varchar(50),
	jour varchar(10),
	heureDebut varchar(10),
	heureFin varchar(10),
	lieu varchar(50),
	description varchar(500),
	foreign key (idProf) references prof(id)
);
insert into discipline values ('',2,'Vogue - Discipline - School Of Wild','Vogue','Cr�� par un homme � l''origine, le vogue ajuste vos mouvements, votre tenue et votre attitude. Soyez classe, jouez et mettez vous dans la peau des plus grands mannequins.','prof-vogue.jpg','photo du prof de vogue','Mercredi','15h30','16h30','Mille et une �toiles Tsimbazaza','Le <strong> voguing </strong> est caract�ris� par la pose mannequin, telle que pratiqu�e dans le magasine am�ricain <strong> Vogue </strong> et lors des d�fil�s de mode, int�gr�e avec des mouvements angulaires, lin�aires des bras et des jambes. Old way, New way et Vogue Fem sont les diff�rents styles de la discipline.');
insert into discipline values ('',1,'Hip Hop - Discipline - School Of Wild','Hip hop','En mode hip hop New style, travaillez votre fluidit�, votre �nergie. D�couvrez les diff�rents feeling, et les bases.','prof-hip-hop.jpg','photo du prof de hip hop','Samedi','16h','17h','Mille et une �toiles Tsimbazaza','La <strong>danse hip hop</strong> ou plus pr�cis�ment <strong>hip hop New style</strong> est une des danses les plus connues et se pratique sur des musiques hip hop, rap ou autres qui sont tr�s rythmiques et comportant beaucoup de basses. Un des types de danse qui est le plus repr�sent� par le biais de clip de Rap et de RnB aux Etats-Unis.');
insert into discipline values ('',2,'Street Jazz - Discipline - School Of Wild','Street jazz','Soyez d�complex� et travaillez votre souplesse. Le street jazz permet d''exprimer la sensualit� et de mettre en avant votre charme et vos atouts f�minins. Mais elle peut �tre pratiqu�e par tous, fille ou gar�on.','prof-street-jazz.jpg','photo du prof de street jazz','Samedi','17h','18h','Mille et une �toiles Tsimbazaza','Une discipline � mi-chemin entre le modern jazz et le hip hop le <strong>street jazz</strong> permet un travail sur l''�nergie, les rythmes syncop�s, les arrets, les silences, la dynamique, tout en favorisant l''expression individuelle et le feeling. Elle fait appel � un style plus <strong>� street �</strong> qui repose beaucoup sur les attitudes <strong>� girly �</strong>');
insert into discipline values ('',1,'Stepping - Discipline - School Of Wild','Stepping','Soyez la musique sur laquelle vous danser, cr�ez des sons avec votre corps. Le step ou stepping vous permet de travailler la coordination de vos mouvements sur le son que vous produisez.','prof-stepping.jpg','photo du prof de stepping','Samedi','16h','17h','Mille et une �toiles Tsimbazaza','Le <strong>stepping</strong> ou <strong>step-dancing</strong> est une discipline o� le corps est utilis� en tant qu''instrument pour produire des sons, des rythmes gr�ce aux � claps �, aux � footsteps �, etc... En plus de produire la musique recherch�, les mouvements ex�cut�s sont la partie � danse � du stepping.');
insert into discipline values ('',2,'Twerk - Discipline - School Of Wild','Twerk','Apprenez les techniques de bases du twerk. Eh oui, il y en a. Savoir bouger les hanches c''est la cl�.','prof-twerk.jpg','photo du prof de twerk','Samedi','14h30','15h30','Mille et une �toiles Tsimbazaza','Le terme est form� � partir de deux lettres initiales de <strong>� twist �</strong> et les trois lettres finales de <strong>� jerk �</strong>, du nom des danses auxquelles il emprunte certains mouvements et d�hanch�s. Le <strong>twerk</strong> ou <strong>twerking</strong> est d�fini comme le fait de � danser de mani�re os�, secouer les hanches et les fesses.�');
insert into discipline values ('',1,'House - Discipline - School Of Wild','House','Apprenez � trouver votre groove avec la house dance. D�couvrez les bases et le feeling de l�g�ret� de cette discipline.','prof-house.jpg','photo du prof de house','Mercredi','14h30','15h30','Mille et une �toiles Tsimbazaza','Tout comme le hip hop, la house music a inspir� sa propre danse, la <strong>danse house</strong>. Elle se traduit par un riche m�lange de pas d''origine tr�s diverses : Salsa, afro, claquettes ... Elle se reconna�t par un style l�ger et a�rien.');
insert into discipline values ('',2,'Dancehall - Discipline - School Of Wild','Dancehall','Apprenez les steps de base, le flow et l''attitude du dancehall. Old School, Middle School et New School, d�couvrez la vybz en mode Bad Man style ou Gyal style.','prof-dancehall.jpg','photo du prof de dancehall','Samedi','14h30','15h30','Mille et une �toiles Tsimbazaza','N� en Jama�que ou plus pr�cis�ment � Kingston au tout d�but des ann�es 1980, le <strong>dancehall</strong> est apparu durant l''�poque des changement sociaux et politiques et s''est rapidement propag� dans les Antilles avant d''atteindre le reste des pays francophones.');
insert into discipline values ('',3,'Afro Fusion - Discipline - School Of Wild','Afro fusion','Alliez joie de vivre, �nergie et danse. D�couvrez les differentes bases de l''afro fusion, la culture et le feeling de la discipline.','prof-afro-fusion.jpg','photo du prof d''afro fusion','Samedi','13h30','14h30','Mille et une �toiles Tsimbazaza','');

create table evenement
(
	id int not null auto_increment primary key,
	titre varchar(20),
	sousTitre varchar(100),
	dateEvenement date,
	photo varchar(400),
	alt varchar(50),
	description varchar(500)
);

insert into evenement values ('','Let''s Dance','Sp�ctacle de fin d''ann�e','2018-07-03','affiche-lets-dance.jpg','affiche de l''�v�nement Let''s Dance','LET�S DANCE a �t� le premier �v�nement organis� par l��cole de danse urbaine School of wild. Elle a pour but de pr�senter le travail des �l�ves  � l�aide du grand spectacle de fin de saison mais aussi d�offrir l�opportunit� � de jeunes groupes de danseurs urbains de partager leur art , de se surpasser gr�ce aux diff�rents concours de danse.  Il est rapidement devenu un �v�nement incontournable dans le milieu de la danse et � sa cinqui�me �dition, qui s�est d�roul� en 2017, il est devenu un �v�nement international  gr�ce � la collaboration avec � Dancehall Master World �.');
insert into evenement values ('','High Dih','Presentation officielle du groupe G','2018-12-24','affiche-high-dih.jpg','affiche de l''�v�nement High Dih','HIGH DIH est un �v�nement de danse qui a pour but de rassembler les chor�graphes et les diff�rents groupes de danse � Antananarivo. Il se divise en deux(2) parties : 
1.	Des workshops de chaque chor�graphe d�un groupe
2.	Spectacle de danse
Pour les workshops, chaque chor�graphe repr�sentant son groupe a une heure pour partager son art, sa discipline de danse et sa particularit�.
Pour le grand spectacle, les groupes feront des shows pour le grand public, passionn� de la danse.
La premi�re �dition en 2016 s�est d�roul� au CCESCA Antanimena, la seconde s�est concr�tis�e en fin d�cembre 2017 au CERCLE MES Soanierana
');
insert into evenement values ('','We Are Wild','Presentation officielle du groupe We Are Wild','2018-04-22','affiche-WAW.jpg','affiche de l''�v�nement du groupe WAW','Pour �largir son contact avec l�ext�rieur, l��cole de danse organisera un �v�nement culturel qui visera � appliquer ce que les �l�ves ont appris depuis la rentr�e. Au programme, on a choisi les six (6) meilleurs �l�ves pour donner un atelier de danse � un public de tout �ge sous forme de battle (nouveau concept), sans aucun professeur suivi de prestations de tous les �l�ves. Afin de travailler le bon relationnel professeurs-�l�ves-public, il y aura diff�rents concours de tout genre, animations et divertissements pour tout �ge.');
insert into evenement values ('','Night By G','Soir�e pr�sentation officielle du groupe G (part 2)','2018-04-28','affiche-night-by-G.jpg','affiche de l''�v�nement Night By G','G ("Ji") est un groupe de danse qui a �t� cr�� en Ao�t 2017, compos� de quatre (6) filles , form� pour mieux faire conna�tre les disciplines suivantes au grand public : Street Jazz, Vogue, Twerk et Waacking. Dans le style sensuel et glamour que d�gage ces disciplines , il se d�marque par une touche particuli�re et propre qui est de danser en chaussettes longues. Apr�s une premi�re soir�e pour la pr�sentation officielle du groupe au Kudeta  Carlton, il continue sa route pour une deuxi�me partie dans la d�couverte de leur art.');

create table aLaUne 
(
	id int not null auto_increment primary key,
	idEvenement int,
	dateEvenement dateTime,
	lieu varchar(50),
	foreign key (idEvenement) references evenement(id)
);
insert into aLaUne values ('',3,'2018-04-22 9:30','Mille et une �toiles Tsimbazaza');
insert into aLaUne values ('',4,'2018-04-28 20:00','La Casa Analakely');

create table admin
(
	id int not null auto_increment primary key,
	pseudo varchar(20),
	mdp varchar(20)
);
insert into admin values ('','root','root');


//WebHost
nom site web : schoolofwild
mdp : schoolofwild
//Bdd
utilisateur bdd : hanitra
mdp : hanitra

//deploiement farany : 15h50 05-04-2018

//Bitbucket
Hanitra Rakoto
hanitra24
username : hanitrarak
