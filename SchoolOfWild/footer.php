<section id="bottom">
	<div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
		<div class="row">
			<div class="col-md-1">
			</div>
			<div class="col-md-4 col-sm-12">
				<div class="widget">
					<h3>Pour voir plus</h3>
					<ul>
						<li><a href="https://ecole-de-danse-grenoble.com/">Ecole de danse grenoble</a></li>
						<li><a href="https://normandhiphop.wordpress.com/ecoles/">Normand Hip Hop</a></li>
						<li><a href="http://studio317.wixsite.com/danse/lecole">STUDIO 317 </a></li>
						<li><a href="http://studiomrg.com/">STUDIO MRG</a></li>                           
					</ul>
				</div>    
			</div><!--/.col-md-3-->

			<div class="col-md-4 col-sm-12">
				<div class="widget">
					<h3>Contact</h3>
					<ul>
						<li>schoolOfWild@gmail.com</li>
						<li>+ 261 33 17 017 17</li>
						<li>+ 261 33 09 009 09</li>
						<li>+ 261 32 02 002 02</li>                     
					</ul>
				</div>    
			</div><!--/.col-md-3-->


			<div class="col-md-2 col-sm-12">
				<div class="widget">
					<h3>Adresse</h3>
					<ul>
						<li>Mille et une étoiles</a></li>
						<li>Tsimbazaza</li>
						<li>Antananarivo</li>
						<li>Madagascar</li>                           
					</ul>
				</div>    
			</div><!--/.col-md-3-->
		</div>
	</div>
	
</section><!--/#bottom-->



<div class="top-bar">
	<div class="container">
		<div class="row">
			<div class="col-md-5"></div>
			<div class="col-md-2">
			   <div class="social">
					<ul id="navlist" style="background-color:red">
						<li id="facebook"><a href="https://www.facebook.com/School-Of-Wild-180245275419050/?refid=46&__xts__%5B0%5D=12.%7B%22unit_id_click_type%22%3A%22graph_search_results_item_tapped%22%2C%22click_type%22%3A%22result%22%2C%22module_id%22%3A1%2C%22result_id%22%3A180245275419050%2C%22session_id%22%3A%223c3a2d10d3857c7322ee227568b6b9d6%22%2C%22module_role%22%3A%22ENTITY_PAGES%22%2C%22unit_id%22%3A%22browse_rl%3A12da43ce-c489-a170-d31c-24e5c91b9ecc%22%2C%22browse_result_type%22%3A%22browse_type_page%22%2C%22unit_id_result_id%22%3A180245275419050%2C%22module_result_position%22%3A0%7D"></a></li>
						<li id="gmail"><a href="contact-contacter-school-of-wild.html"></a></li>
						<li id="instagram"><a href="https://www.instagram.com/hanitra_rak/?hl=fr"></a></li>
					</ul>
			   </div>
			</div>
			<div class="col-md-6"></div>
		</div>
	</div><!--/.container-->
</div><!--/.top-bar-->

<footer id="footer" class="midnight-blue">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				&copy; 2018 Hanitra Rakoto. Prom 9A ETU000516.
			</div>
			<!-- 
				All links in the footer should remain intact. 
				Licenseing information is available at: http://bootstraptaste.com/license/
				You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Gp
			-->
			<div class="col-sm-6">
				<ul class="pull-right">
					<li><a href="index.php">Accueil</a></li>
					<li><a href="apropos.php">A propos</a></li>
					<li><a href="contact.php">Contact</a></li>
				</ul>
			</div>
		</div>
	</div>
</footer><!--/#footer-->