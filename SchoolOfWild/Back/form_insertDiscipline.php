<?php
	$admin=$_GET['admin'];
	session_start();
	if($_SESSION[$admin]<>"OK") { ?>
		<script type="text/javascript">
			alert("Veuillez vous connecter!");
			document.location.href='index.php'; 
		</script>	
	<?php } 
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Inserer discipline - Back Office School Of Wild</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  
  <?php
	require('../fonctions.php');
	$profs = get_Prof();
  ?>
  
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Inserer Discipline</div>
      <div class="card-body">
	  
	  <form method="post" action="insertDiscipline.php" enctype="multipart/form-data">
		  <div class="form-group">
			<div class="form-row">
			  <div class="col-md-6">
				<input class="form-control" name="admin" type="hidden" required="required" value="<?php echo $admin; ?>">
				<label for="exampleInputName">Discipline</label>
				<input class="form-control" name="discipline" type="text" required="required">
			  </div>
			  <div class="col-md-6">
				<label for="exampleInputLastName">Photo</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input  name="userFile" required="required" type="file">
				</div>
			  </div>
			</div>
		  </div>
		  
		  <div class="form-group">
			<div class="form-row">
			  <div class="col-md-6">
				<label for="exampleInputName">Alt photo</label>
				<input class="form-control" name="alt" type="text" required="required">
			  </div>
			  <div class="col-md-6">
				<label for="exampleConfirmPassword">Prof</label>
				<select class="select2_single form-control" tabindex="-1" name="prof">
					<?php foreach($profs as $prof) { ?>
						<option value="<?php echo $prof['id']; ?>"><?php echo $prof['prenom']; ?></option>
					<?php } ?>
				</select>	
			  </div>
			</div>
		  </div>
		  
		  <div class="form-group">
			<div class="form-row">
			  <div class="col-md-6">
				<label for="exampleInputName">Title page</label>
				<input class="form-control" name="title" type="text" required="required">
			  </div>
			  <div class="col-md-6">
				<label for="exampleConfirmPassword">Jour cours</label>
				<input class="form-control" name="jour" type="text" required="required">
			  </div>
			</div>
		  </div>
		  
		  <div class="form-group">
			<div class="form-row">
			  <div class="col-md-6">
				<label for="exampleInputName">Heure début cours</label>
				<input class="form-control" name="heureDebut" type="text" required="required">
			  </div>
			  <div class="col-md-6">
				<label for="exampleConfirmPassword">Heure fin cours</label>
				<input class="form-control" name="heureFin" type="text" required="required">
			  </div>
			</div>
		  </div>
		  
		  <div class="form-group">
            <label for="exampleInputEmail1">Sous titre</label>
            <textarea class="form-control" name="sousTitre" type="text" required="required"></textarea>
          </div>
		  
		  <div class="form-group">
            <label for="exampleInputEmail1">Description</label>
            <textarea class="form-control" name="description" type="text" required="required"></textarea>
          </div>
          
		  <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <input class="btn btn-primary btn-block" type="submit" value="Valider" >
              </div>
			  <div class="col-md-6">
                <a class="btn btn-danger btn-block" href="tables_galeries.php?admin=<?php echo $admin; ?>">Annuler</a>
              </div>
            </div>
          </div>
        </form>
        
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/popper/popper.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
