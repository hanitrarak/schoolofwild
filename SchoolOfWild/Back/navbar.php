<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="tables_menuSite.php?admin=<?php echo $admin; ?>">Backoffice Scool Of Wild</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menu site">
          <a class="nav-link" href="tables_aLaUne.php?admin=<?php echo $admin; ?>">
            <i class="fa fa-fw fa-reorder"></i>
            <span class="nav-link-text">A la une</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="A propos">
          <a class="nav-link" href="tables_profs.php?admin=<?php echo $admin; ?>">
            <i class="fa fa-fw fa-address-book"></i>
            <span class="nav-link-text">Profs</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menu resto">
          <a class="nav-link" href="tables_disciplines.php?admin=<?php echo $admin; ?>">
            <i class="fa fa-fw fa-cutlery"></i>
            <span class="nav-link-text">Disciplines</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Galerie">
          <a class="nav-link" href="page_accueil.php?admin=<?php echo $admin; ?>">
            <i class="fa fa-fw fa-photo"></i>
            <span class="nav-link-text">Page Accueil</span>
          </a>
        </li>
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Chefs">
          <a class="nav-link" href="page_apropos.php?admin=<?php echo $admin; ?>">
            <i class="fa fa-fw fa-graduation-cap"></i>
            <span class="nav-link-text">Page a propos</span>
          </a>
        </li>
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Chefs">
          <a class="nav-link" href="page_contact.php?admin=<?php echo $admin; ?>">
            <i class="fa fa-fw fa-graduation-cap"></i>
            <span class="nav-link-text">Page contact</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Se deconnecter</a>
        </li>
      </ul>
    </div>
  </nav>