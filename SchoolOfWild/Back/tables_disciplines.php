<?php
	$admin=$_GET['admin'];
	session_start();
	if($_SESSION[$admin]<>"OK") { ?>
		<script type="text/javascript">
			alert("Veuillez vous connecter!");
			document.location.href='index.php'; 
		</script>	
	<?php } 
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Table disciplines - Back Office School Of Wild</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  
  <script src="../js/fonction.js"></script>
  
  <?php require ('../fonctions.php') ?>
  <?php 
	$disciplines = get_DisciplineProf();
  ?>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <?php include('navbar.php') ?>
  
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item" style="color: dodgerblue">Backoffice</li>
        </li>
        <li class="breadcrumb-item active">Table Disciplines</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>Table Disciplines <a href="form_insertDiscipline.php?admin=<?php echo $admin; ?>" class="btn btn-primary btn-xs"> Ajouter </a></div>
        <div class="card-body">
          <div class="table-responsive" style="margin-left:-20px">
            <table style="margin-left:-15px" class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Discipline</th>
                  <th>Photo / Alt</th>
				  <th>Prof</th>
                  <th>Title</th>
				  <th>Sous titre</th>
                  <th>Heure</th>
                  <th>Description</th>
                  <th></th>
                </tr>
              </thead>
              <tfoot>
                <tr>
				  <th>Discipline</th>
                  <th>Photo / Alt</th>
				  <th>Prof</th>
                  <th>Title</th>
                  <th>Sous titre</th>
                  <th>Heure</th>
                  <th>Description</th>
                  <th></th>
                </tr>
              </tfoot>
              <tbody>
				<?php foreach ($disciplines as $discipline) { ?>
                <tr>
				  <td><?php echo $discipline['discipline']; ?></td>
				  <td><img src="../images/prof/<?php echo $discipline['photo']; ?>" width="60" height="60"/><?php echo $discipline['alt']; ?></td>
				  <td><?php echo $discipline['prenom']; ?></td>
                  <td><?php echo $discipline['title']; ?></td>
				  <td><?php echo $discipline['sousTitre']; ?></td>
                  <td><?php echo $discipline['jour']." "; ?><?php echo $discipline['heureDebut']; ?> - <?php echo $discipline['heureFin']; ?></td>
                  <td><?php echo $discipline['description']; ?></td>
                  <td>
					<a href="form_modifDiscipline.php?admin=<?php echo $admin; ?>&id=<?php echo $discipline['idDiscipline']; ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                    <a href="deleteDiscipline.php?admin=<?php echo $admin; ?>&id=<?php echo $discipline['idDiscipline']; ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
				  </td>
                </tr>
				<?php } ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    
	<?php include('footer.php') ?>
	
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>
  </div>
</body>

</html>
