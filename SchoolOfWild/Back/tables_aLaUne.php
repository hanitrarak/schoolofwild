<?php
	$admin=$_GET['admin'];
	session_start();
	if($_SESSION[$admin]<>"OK") { ?>
		<script type="text/javascript">
			alert("Veuillez vous connecter!");
			document.location.href='index.php'; 
		</script>	
	<?php } 
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Table A la Une - Backoffice School Of Wild</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  
  <script src="../js/fonction.js"></script>
  
  <?php require ('../fonctions.php') ?>
  <?php 
	$aLaUnes = get_AlaUneEvenement();
	$evenements = get_Evenement();
  ?>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <?php include('navbar.php') ?>
  
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item" style="color: dodgerblue">Backoffice</li>
        <li class="breadcrumb-item active">Table A la Une</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>Table A la Une</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Evénement</th>
                  <th>Date de l'événement</th>
                  <th>Lieu</th>
                  <th></th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Evénement</th>
                  <th>Date de l'événement</th>
                  <th>Lieu</th>
                  <th></th>
                </tr>
              </tfoot>
              <tbody>
				<?php foreach ($aLaUnes as $aLaUne) { ?>
                <tr>
                  <td><?php echo $aLaUne['titre']; ?></td>
                  <td><?php echo $aLaUne['dateEvenement']; ?></td>
                  <td><?php echo $aLaUne['lieu']; ?></td>
                  <td>
					<a href="form_modifAlaUne.php?admin=<?php echo $admin; ?>&id=<?php echo $aLaUne['idAlaUne'] ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                    <a href="deleteAlaUne.php?admin=<?php echo $admin; ?>&id=<?php echo $aLaUne['idAlaUne'] ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
				  </td>
                </tr>
				<?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
	  
	  
	  <div class="card card-register mx-auto mt-5">
      <div class="card-header">Inserer nouveau événement à la une</div>
      <div class="card-body">
        <form method="post" action="insertAlaUne.php" enctype="multipart/form-data">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-12">
                <label for="exampleInputName">Evénement</label>
				<input class="form-control" name="admin" type="hidden" required="required" value="<?php echo $admin; ?>">
                <select class="select2_single form-control" tabindex="-1" name="evenement">
					<?php foreach($evenements as $evenement) { ?>
						<option value="<?php echo $evenement['id']; ?>"><?php echo $evenement['titre']; ?></option>
					<?php } ?>
				</select>	
              </div>
            </div>
          </div>
		  <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputName">Date de l'événement</label>
				<input class="form-control" name="dateEvenement" type="text" required="required">
              </div>
			  <div class="col-md-6">
                <label for="exampleInputName">Lieu de l'événement</label>
				<input class="form-control" name="lieu" type="text" required="required">
              </div>
            </div>
          </div>
		  <div class="form-group">
            <div class="form-row">
              <div class="col-md-12">
                <input class="btn btn-primary btn-block" type="submit" value="Valider" >
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
	  
	  
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    
	<?php include('footer.php') ?>
	
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>
  </div>
</body>

</html>
