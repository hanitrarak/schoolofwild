<?php
	$admin=$_GET['admin'];
	session_start();
	if($_SESSION[$admin]<>"OK") { ?>
		<script type="text/javascript">
			alert("Veuillez vous connecter!");
			document.location.href='index.php'; 
		</script>	
	<?php } 
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Table Prof- Backoffice School Of Wild</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  
  <script src="../js/fonction.js"></script>
  
  <?php require ('../fonctions.php') ?>
  <?php 
	$profs = get_Prof();
  ?>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <?php include('navbar.php') ?>
  
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item" style="color: dodgerblue">Backoffice</li>
        <li class="breadcrumb-item active">Table Prof</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>Table Prof</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Prenom</th>
                  <th></th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Id</th>
                  <th>Prenom</th>
                  <th></th>
                </tr>
              </tfoot>
              <tbody>
				<?php foreach ($profs as $prof) { ?>
                <tr>
                  <td><?php echo $prof['id']; ?></td>
                  <td><?php echo $prof['prenom']; ?></td>
                  <td>
                    <a href="deleteProf.php?admin=<?php echo $admin; ?>&id=<?php echo $prof['id'] ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
				  </td>
                </tr>
				<?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
	  
	  
	  <div class="card card-register mx-auto mt-5">
      <div class="card-header">Inserer Nouveau Prof</div>
      <div class="card-body">
        <form method="post" action="insertProf.php" enctype="multipart/form-data">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-12">
                <label for="exampleInputName">Prenom</label>
				<input class="form-control" name="admin" type="hidden" required="required" value="<?php echo $admin; ?>">
                <input class="form-control" name="prenom" type="text" required="required" placeholder="Entrer prenom">
              </div>
            </div>
          </div>
		  <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <input class="btn btn-primary btn-block" type="submit" value="Valider" >
              </div>
			  <div class="col-md-6">
                <a class="btn btn-danger btn-block" href="tables_menus.php?admin=<?php echo $admin; ?>">Annuler</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
	  
	  
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    
	<?php include('footer.php') ?>
	
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>
  </div>
</body>

</html>
