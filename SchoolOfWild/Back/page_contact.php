<?php
	$admin=$_GET['admin'];
	session_start();
	if($_SESSION[$admin]<>"OK") { ?>
		<script type="text/javascript">
			alert("Veuillez vous connecter!");
			document.location.href='index.php';
		</script>	
	<?php } 
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Page Contact - Backoffice School Of Wild</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  
  <script src="../js/fonction.js"></script>
  
  <?php require ('../fonctions.php') ?>
  <?php 
	$contact = get_Contact();
	$title = $contact[0]['title'];
	$h1 = $contact[0]['h1'];
	$h2 = $contact[0]['h2'];
	$url = $contact[0]['url'];
  ?>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <?php include('navbar.php') ?>
  
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item" style="color: dodgerblue">Backoffice</li>
        <li class="breadcrumb-item active">Page Contact</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>Page Contact</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>H1</th>
                  <th>H2</th>
                  <th>url</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Title</th>
                  <th>H1</th>
                  <th>H2</th>
				  <th>url</th>
                </tr>
              </tfoot>
              <tbody>
				
                <tr>
                  <td><?php echo $title; ?></td>
                  <td><?php echo $h1; ?></td>
                  <td><?php echo $h2; ?></td>
                  <td><?php echo $url; ?></td>
                </tr>
				
              </tbody>
            </table>
          </div>
        </div>
      </div>
	  
	  
	  <div class="card card-register mx-auto mt-5">
      <div class="card-header">Modifier page contact</div>
      <div class="card-body">
        <form method="post" action="modifContact.php" enctype="multipart/form-data">
		  <div class="form-group">
            <div class="form-row">
              <div class="col-md-12">
                <label for="exampleInputName">Title</label>
				<input class="form-control" name="admin" type="hidden" value="<?php echo $admin ?>" required="required">
				<input class="form-control" name="title" type="text" value="<?php echo $title ?>" required="required">
              </div>
            </div>
          </div>
		  <div class="form-group">
            <div class="form-row">
              <div class="col-md-12">
                <label for="exampleInputName">H1</label>
				<input class="form-control" name="h1" type="text" value="<?php echo $h1 ?>" required="required">
              </div>
            </div>
          </div>
		  <div class="form-group">
            <div class="form-row">
              <div class="col-md-12">
                <label for="exampleInputName">H2</label>
				<input class="form-control" name="h2" type="text" value="<?php echo $h2 ?>" required="required">
              </div>
            </div>
          </div>
		  <div class="form-group">
            <div class="form-row">
              <div class="col-md-12">
                <label for="exampleInputName">url</label>
				<input class="form-control" name="url" type="text" value="<?php echo $url ?>" required="required">
              </div>
            </div>
          </div>
		  <div class="form-group">
            <div class="form-row">
              <div class="col-md-12">
                <input class="btn btn-primary btn-block" type="submit" value="Modifier" >
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
	  
	  
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    
	<?php include('footer.php') ?>
	
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>
  </div>
</body>

</html>
