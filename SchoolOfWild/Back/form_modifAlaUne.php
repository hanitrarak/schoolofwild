<?php
	$admin=$_GET['admin'];
	session_start();
	if($_SESSION[$admin]<>"OK") { ?>
		<script type="text/javascript">
			alert("Veuillez vous connecter!");
			document.location.href='index.php'; 
		</script>	
	<?php } 
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Mofifier a la une - Backoffice School Of Wild</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  
  <?php require ('../fonctions.php') ?>
  <?php 
	$id = $_GET['id'];
	$aLaUnes = get_AlaUneById($id);
	$evenements = get_Evenement();
	if ($aLaUnes == NULL){
	echo $id;
	}
	
  ?>
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Mofifier a la une</div>
      <div class="card-body">
        <form method="post" action="modifAlaUne.php" enctype="multipart/form-data">
		  <?php foreach ($aLaUnes as $aLaUne) { ?>
		  <div class="form-group">
            <div class="form-row">
              <div class="col-md-12">
                <label for="exampleInputName">Evénement</label>
				<input class="form-control" name="admin" type="hidden" required="required" value="<?php echo $admin; ?>">
				<input class="form-control" name="id" type="hidden" required="required" value="<?php echo $aLaUne['id']; ?>">
                <select class="select2_single form-control" tabindex="-1" name="evenement">
					<?php foreach($evenements as $evenement) { ?>
						<option value="<?php echo $evenement['id']; ?>"><?php echo $evenement['titre']; ?></option>
					<?php } ?>
				</select>	
              </div>
            </div>
          </div>
		  <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputName">Date de l'événement</label>
				<input class="form-control" name="dateEvenement" type="text" value="<?php echo $aLaUne['dateEvenement'] ?>" required="required">
              </div>
			  <div class="col-md-6">
                <label for="exampleInputName">Lieu de l'événement</label>
				<input class="form-control" name="lieu" type="text" value="<?php echo $aLaUne['lieu'] ?>" required="required">
              </div>
            </div>
          </div>
		  <div class="form-group">
            <div class="form-row">
              <div class="col-md-12">
                <input class="btn btn-primary btn-block" type="submit" value="Valider" >
              </div>
            </div>
          </div>
		  <?php } ?>
        </form>
        
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/popper/popper.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
