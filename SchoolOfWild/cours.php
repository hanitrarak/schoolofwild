<!DOCTYPE html>
<html lang="en">
  <head>
  
	<?php 
		require('fonctions.php');
		$idCour = $_GET['id'];
		$cours = get_DisciplineById($idCour);
		$spit = splitEspace($cours[0]['discipline']);
		$url = "cours-".$spit;
		/*echo $_GET['url']."</br>";
		echo $url."</br>";
		echo $url.'-'.$idCour.'.html';*/
		if (strtolower($_GET['url']) != strtolower($url) ) {
			header('location:'.$url.'-'.$idCour.'.html');
		}
	?>
	
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content='<?php echo $cours[0]['description'] ?>'>
    <title><?php echo $cours[0]['title'] ?></title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">      
	<link href="css/main.css" rel="stylesheet">
	 <link href="css/responsive.css" rel="stylesheet">
	 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    
  </head>
  <body class="homepage">   
	<?php include('header.php'); ?>
	
	<section id="services" class="service-item">
	   <div class="container">
			<?php foreach($cours as $cour) { ?>
            <div class="center wow fadeInDown">
                <h1 style="text-transform : uppercase; font-size:50px"><?php echo $cour['discipline']; ?></h1>
                <div class="row">
					<div class="col-md-2">
					</div>
					<div class="col-sm-6 col-md-8">
						<h2 style="font-size:18px"><?php echo $cour['sousTitre']; ?></h2>
					</div>
					<div class="col-md-2">
					</div>
				</div>
            </div>

            <div class="row">
				<div class="col-md-2">
				</div>
                <div class="col-sm-6 col-md-8">
                    <div class="media services-wrap wow fadeInDown">
                        
                        <div class="media-body">
							<div class="row">
								<div class="col-md-4">
									<img class="img-responsive" src="images/prof/<?php echo $cour['photo'] ?>" alt="<?php echo $cour['alt'] ?>" title="<?php echo $cour['alt'] ?>" width="100%">
								</div>
								<div class="col-md-8">
									<h3 style="color:black; font-size:18px; font-family:calibri"><?php echo $cour['description']; ?></h3>
									<p>Prof : <?php echo $cour['prenom'] ?></p>
									<p>Jour de cours : <?php echo $cour['jour'] ?></p>
									<p>Heure début : <?php echo $cour['heureDebut']; ?></p>
									<p>Heure fin : <?php echo $cour['heureFin']; ?></p>
									<p>Lieu : <?php echo $cour['lieu'] ?></p>
								</div>
							</div>
                        </div>
                    </div>
                </div>
				<div class="col-md-2">
				</div>
				
            </div><!--/.row-->
			<?php } ?>
        </div><!--/.container-->
    </section><!--/#services-->

	<?php include('footer.php'); ?>
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>   
    <script src="js/wow.min.js"></script>
	<script src="js/main.js"></script>
  </body>
</html>