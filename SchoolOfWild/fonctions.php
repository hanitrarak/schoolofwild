<?php
function dbconnect()
{
	$user='root';
    $pass='root';
    $dsn='mysql:host=localhost;port=3306;dbname=school';
    static $dbh = null;
    if ($dbh === null) {
        $dbh = new PDO($dsn, $user, $pass);
		$dbh->exec("set names utf8");
    }
    return $dbh;
}

function get_Menu()
{
	$resultats=dbconnect()->query("select * from menu");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}

function get_Discipline()
{
	$resultats=dbconnect()->query("select * from discipline");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}

function get_Evenement()
{
	$resultats=dbconnect()->query("select * from evenement");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}

function get_AlaUne()
{
	$resultats=dbconnect()->query("select * from aLaUne a join evenement e on a.idEvenement = e.id");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}

function get_AlaUneEvenement()
{
	$resultats=dbconnect()->query("select a.id as idAlaUne, a.idEvenement, e.titre, a.dateEvenement, a.lieu from aLaUne a join evenement e on a.idEvenement = e.id");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}

function get_Prof()
{
	$resultats=dbconnect()->query("select * from prof");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}

function get_DisciplineById($id)
{
	$resultats=dbconnect()->query("select * from discipline d join prof p on d.idProf = p.id where d.idDiscipline = ".$id);
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}

function get_Presentation()
{
	$resultats=dbconnect()->query("select * from presentation");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}

function get_Accueil()
{
	$resultats=dbconnect()->query("select * from accueil");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}

function get_Apropos()
{
	$resultats=dbconnect()->query("select * from apropos");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}

function get_Contact()
{
	$resultats=dbconnect()->query("select * from contact");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}

function splitEspace($discipline) {
	$split = explode(" ", $discipline);
	if (sizeof($split) > 1) {
		$discipline = $split[0]."-".$split[1];
	}
	return $discipline;
}

//Back
function get_Admin($pseudo, $mdp)
{
	$resultats=dbconnect()->query("select * from admin where pseudo='".$pseudo."' and mdp='".$mdp."'");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}

function get_DisciplineProf()
{
	$resultats=dbconnect()->query("select * from discipline d join prof p on d.idProf = p.id");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}

function get_AlaUneById($id)
{
	$resultats=dbconnect()->query("select * from aLaUne where id ='".$id."'");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
	$ligne=$resultats->fetchAll();
	return $ligne;
}

function insertDiscipline($idProf,$title,$discipline,$sousTitre,$photo,$alt,$jour,$heureDebut,$heureFin,$lieu,$description)
{
	dbconnect()->exec('INSERT INTO discipline VALUES ("","'.$idProf.'","'.$title.'","'.$discipline.'","'.$sousTitre.'","'.$photo.'","'.$alt.'","'.$jour.'","'.$heureDebut.'","'.$heureFin.'","'.$lieu.'","'.$description.'")');
}

function updateDiscipline($id,$idProf,$title,$discipline,$sousTitre,$photo,$alt,$jour,$heureDebut,$heureFin,$lieu,$description)
{
	dbconnect()->exec('UPDATE discipline SET idProf="'.$idProf.'",title="'.$title.'",discipline="'.$discipline.'",sousTitre="'.$sousTitre.'",photo="'.$photo.'",alt="'.$alt.'",jour="'.$jour.'",heureDebut="'.$heureDebut.'",heureFin="'.$heureFin.'",lieu="'.$lieu.'",description="'.$description.'" where idDiscipline="'.$id.'"');
}

function deleteDiscipline($id)
{
	$resultats=dbconnect()->query("DELETE FROM discipline WHERE idDiscipline = '".$id."'");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
}

function insertProf($prenom)
{
	dbconnect()->exec('INSERT INTO prof VALUES ("","'.$prenom.'")');
}

function updateProf($id,$prenom)
{
	dbconnect()->exec('UPDATE prof SET prenom="'.$prenom.'" where id="'.$id.'"');
}

function deleteProf($id)
{
	$resultats=dbconnect()->query("DELETE FROM prof WHERE id = '".$id."'");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
}

function insertAlaUne($idEvenement, $dateEvenement, $lieu)
{
	dbconnect()->exec('INSERT INTO aLaUne VALUES ("","'.$idEvenement.'","'.$dateEvenement.'","'.$lieu.'")');
}

function updateAlaUne($id,$idEvenement, $dateEvenement, $lieu)
{
	dbconnect()->exec('UPDATE aLaUne SET idEvenement="'.$idEvenement.'",dateEvenement="'.$dateEvenement.'",lieu="'.$lieu.'" where id="'.$id.'"');
}

function deleteAlaUne($id)
{
	$resultats=dbconnect()->query("DELETE FROM aLaUne WHERE id = '".$id."'");
	$resultats->setFetchMode(PDO::FETCH_ASSOC);
	$resultats->execute();
}


function updateAccueil($title,$h1, $h2, $h3, $url)
{
	dbconnect()->exec('UPDATE accueil SET title="'.$title.'",h1="'.$h1.'",h2="'.$h2.'",h3="'.$h3.'",url="'.$url.'"');
}

function updateApropos($title,$h1, $h21, $h31, $h22, $h32, $url)
{
	dbconnect()->exec('UPDATE apropos SET title="'.$title.'",h1="'.$h1.'",h21="'.$h21.'",h31="'.$h31.'",h22="'.$h22.'",h32="'.$h32.'",url="'.$url.'"');
}

function updateContact($title, $h1, $h2, $url)
{
	dbconnect()->exec('UPDATE contact SET title="'.$title.'",h1="'.$h1.'",h2="'.$h2.'",url="'.$url.'"');
}

?>