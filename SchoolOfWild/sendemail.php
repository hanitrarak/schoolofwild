<?php
// Check for empty fields
if(empty($_POST['nom'])  		||
   empty($_POST['email']) 		||
   empty($_POST['message'])	||
   !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
   {
	echo "No arguments Provided!";
	return false;
   }
	
 $name = $_POST['nom'];
 $destinataire = 'hanitrarakoto531@gmail.com';
 // Pour les champs $expediteur / $copie / $destinataire, séparer par une virgule s'il 
 $expediteur = $_POST['email'];
 // $copie = 'adresse@fai.com'; 
 // $copie_cachee = 'adresse@fai.com'; 
 $objet = "Website Contact Form:  $name";
 // Objet du message 
 $headers  = 'MIME-Version: 1.0' . "\n"; 
 // Version MIME 
 $headers .= 'Reply-To: '.$expediteur."\n"; 
 // Mail de reponse 
 $headers .= 'From: "'.$name.'"<'.$expediteur.'>'."\n"; 
 // Expediteur 
 $headers .= 'Delivered-to: '.$destinataire."\n"; 
 // Destinataire 
 // $headers .= 'Cc: '.$copie."\n"; 
 // Copie Cc 
 // $headers .= 'Bcc: '.$copie_cachee."\n\n"; 
 // Copie cachée Bcc        
 $message = $_POST['message'];
 if (mail($destinataire, $objet, $message, $headers)) 
	 // Envoi du message 
	 {    return true;	
	 } else // Non envoyé 
	 {    echo "Votre message n'a pas pu être envoyé"; 
 }
 header('location:contact.php');
?>