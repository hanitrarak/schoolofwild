﻿-- phpMyAdmin SQL Dump
-- version 4.0.2
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Ven 06 Avril 2018 à 09:03
-- Version du serveur: 5.6.11-log
-- Version de PHP: 5.3.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `id5295867_school`
--
CREATE DATABASE IF NOT EXISTS `id5295867_school` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `id5295867_school`;

-- --------------------------------------------------------

--
-- Structure de la table `accueil`
--

CREATE TABLE IF NOT EXISTS `accueil` (
  `title` varchar(100) DEFAULT NULL,
  `h1` varchar(100) DEFAULT NULL,
  `h2` varchar(100) DEFAULT NULL,
  `h3` varchar(300) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `accueil`
--

INSERT INTO `accueil` (`title`, `h1`, `h2`, `h3`, `url`) VALUES('School Of Wild - Ecole de danse urbaine', 'SCHOOL OF WILD </br> ecole de danse urbaine', 'Les événements organisés par School Of Wild', 'Les billets des événements sont déjà en vente auprès des élèves de <strong> School Of Wild </strong> et au Mille et une étoiles Tsimbazaza', 'accueil-school-of-wild-ecole-de-danse-urbaine');

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(20) DEFAULT NULL,
  `mdp` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `admin`
--

INSERT INTO `admin` (`id`, `pseudo`, `mdp`) VALUES(1, 'root', 'root');

-- --------------------------------------------------------

--
-- Structure de la table `alaune`
--

CREATE TABLE IF NOT EXISTS `alaune` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idEvenement` int(11) DEFAULT NULL,
  `dateEvenement` datetime DEFAULT NULL,
  `lieu` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idEvenement` (`idEvenement`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `alaune`
--

INSERT INTO `alaune` (`id`, `idEvenement`, `dateEvenement`, `lieu`) VALUES(8, 4, '2018-04-28 20:00:00', 'La Casa Analakely');
INSERT INTO `alaune` (`id`, `idEvenement`, `dateEvenement`, `lieu`) VALUES(7, 3, '2018-05-24 14:00:00', 'Dome rta');

-- --------------------------------------------------------

--
-- Structure de la table `apropos`
--

CREATE TABLE IF NOT EXISTS `apropos` (
  `title` varchar(100) DEFAULT NULL,
  `h1` varchar(100) DEFAULT NULL,
  `h21` varchar(100) DEFAULT NULL,
  `h31` varchar(800) DEFAULT NULL,
  `h22` varchar(100) DEFAULT NULL,
  `h32` varchar(800) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `apropos`
--

INSERT INTO `apropos` (`title`, `h1`, `h21`, `h31`, `h22`, `h32`, `url`) VALUES('Présentation de l''école School Of Wild - School Of Wild', 'PRESENTATION DE L''ECOLE SCHOOL OF WILD', 'Historique de l''école', '<strong> School of wild </strong>, crée en 2011, est la première école de danse purement urbaine à Madagascar. Mené par <strong> Andriamahery Frédérico </strong> du groupe <strong> GB Wild </strong>, elle ne se limite pas seulement à l''apprentissage de la danse avec des cours collectifs ou particuliers mais à un partage total dans le but de faire connaître un peu plus la danse au grand public grâce à differents concept d''événements. Depuis 2013 elle a réussi à se placer en tant que pionnier dans ce milieu avec son premier événement <strong> « Let''s Dance » </strong> qui s''est vu devenir événement national puis international depuis 2017; avec la collaboration de « Dancehall Master World » ; « High Dih » qui a concrétisé sa 2ème édition en fin décembre 2017 et d''autres à venir...', 'Quelques photos de la School Of Wild', 'Plus qu''une école de danse, <strong> School Of Wild </strong> propose différents types de services pour des événements de tout genre. Diverses animations de mariage, anniversaire, des After work pour les entreprises, etc ... </br> Des spectacles, des shows mais aussi des tournages publicitaires. </br> Elle a déjà travaillé avec différentes entreprises telles que <strong> Orange </strong>,<strong> Vero Vero Hair </strong>, <strong> Galax </strong>, et en collaboration étroite avec différents médias comme <strong> Dream''in </strong>, <strong> Record </strong> et <strong> Viva </strong>.', 'apropos-presentation-de-l-ecole-school-of-wild');

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `title` varchar(100) DEFAULT NULL,
  `h1` varchar(100) DEFAULT NULL,
  `h2` varchar(200) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `contact`
--

INSERT INTO `contact` (`title`, `h1`, `h2`, `url`) VALUES('Contact - School Of Wild', 'Contacter School Of Wild', 'Contactez-nous pour plus d''information ou si vous avez des questions.', 'contact-contacter-school-of-wild');

-- --------------------------------------------------------

--
-- Structure de la table `discipline`
--

CREATE TABLE IF NOT EXISTS `discipline` (
  `idDiscipline` int(11) NOT NULL AUTO_INCREMENT,
  `idProf` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `discipline` varchar(20) DEFAULT NULL,
  `sousTitre` varchar(300) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `alt` varchar(50) DEFAULT NULL,
  `jour` varchar(10) DEFAULT NULL,
  `heureDebut` varchar(10) DEFAULT NULL,
  `heureFin` varchar(10) DEFAULT NULL,
  `lieu` varchar(50) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idDiscipline`),
  KEY `idProf` (`idProf`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `discipline`
--

INSERT INTO `discipline` (`idDiscipline`, `idProf`, `title`, `discipline`, `sousTitre`, `photo`, `alt`, `jour`, `heureDebut`, `heureFin`, `lieu`, `description`) VALUES(1, 2, 'Vogue - Discipline - School Of Wild', 'Vogue', 'Créé par un homme à l''origine, le vogue ajuste vos mouvements, votre tenue et votre attitude. Soyez classe, jouez et mettez vous dans la peau des plus grands mannequins.', 'prof-vogue.jpg', 'photo du prof de vogue', 'Mercredi', '15h30', '16h30', 'Mille et une étoiles Tsimbazaza', 'Le <strong> voguing </strong> est caractérisé par la pose mannequin, telle que pratiquée dans le magasine américain <strong> Vogue </strong> et lors des défilés de mode, intégrée avec des mouvements angulaires, linéaires des bras et des jambes. Old way, New way et Vogue Fem sont les différents styles de la discipline.');
INSERT INTO `discipline` (`idDiscipline`, `idProf`, `title`, `discipline`, `sousTitre`, `photo`, `alt`, `jour`, `heureDebut`, `heureFin`, `lieu`, `description`) VALUES(2, 1, 'Hip Hop - Discipline - School Of Wild', 'Hip hop', 'En mode hip hop New style, travaillez votre fluidité, votre énergie. Découvrez les différents feeling, et les bases.', 'prof-hip-hop.jpg', 'photo du prof de hip hop', 'Samedi', '16h', '17h', 'Mille et une étoiles Tsimbazaza', 'La <strong>danse hip hop</strong> ou plus précisément <strong>hip hop New style</strong> est une des danses les plus connues et se pratique sur des musiques hip hop, rap ou autres qui sont très rythmiques et comportant beaucoup de basses. Un des types de danse qui est le plus représenté par le biais de clip de Rap et de RnB aux Etats-Unis.');
INSERT INTO `discipline` (`idDiscipline`, `idProf`, `title`, `discipline`, `sousTitre`, `photo`, `alt`, `jour`, `heureDebut`, `heureFin`, `lieu`, `description`) VALUES(3, 2, 'Street Jazz - Discipline - School Of Wild', 'Street jazz', 'Soyez décomplexé et travaillez votre souplesse. Le street jazz permet d''exprimer la sensualité et de mettre en avant votre charme et vos atouts féminins. Mais elle peut être pratiquée par tous, fille ou garçon.', 'prof-street-jazz.jpg', 'photo du prof de street jazz', 'Samedi', '17h', '18h', 'Mille et une étoiles Tsimbazaza', 'Une discipline à mi-chemin entre le modern jazz et le hip hop le <strong>street jazz</strong> permet un travail sur l''énergie, les rythmes syncopés, les arrets, les silences, la dynamique, tout en favorisant l''expression individuelle et le feeling. Elle fait appel à un style plus <strong>« street »</strong> qui repose beaucoup sur les attitudes <strong>« girly »</strong>');
INSERT INTO `discipline` (`idDiscipline`, `idProf`, `title`, `discipline`, `sousTitre`, `photo`, `alt`, `jour`, `heureDebut`, `heureFin`, `lieu`, `description`) VALUES(4, 1, 'Stepping - Discipline - School Of Wild', 'Stepping', 'Soyez la musique sur laquelle vous danser, créez des sons avec votre corps. Le step ou stepping vous permet de travailler la coordination de vos mouvements sur le son que vous produisez.', 'prof-stepping.jpg', 'photo du prof de stepping', 'Samedi', '16h', '17h', 'Mille et une étoiles Tsimbazaza', 'Le <strong>stepping</strong> ou <strong>step-dancing</strong> est une discipline où le corps est utilisé en tant qu''instrument pour produire des sons, des rythmes grâce aux « claps », aux « footsteps », etc... En plus de produire la musique recherché, les mouvements exécutés sont la partie « danse » du stepping.');
INSERT INTO `discipline` (`idDiscipline`, `idProf`, `title`, `discipline`, `sousTitre`, `photo`, `alt`, `jour`, `heureDebut`, `heureFin`, `lieu`, `description`) VALUES(5, 2, 'Twerk - Discipline - School Of Wild', 'Twerk', 'Apprenez les techniques de bases du twerk. Eh oui, il y en a. Savoir bouger les hanches c''est la clé.', 'prof-twerk.jpg', 'photo du prof de twerk', 'Samedi', '14h30', '15h30', 'Mille et une étoiles Tsimbazaza', 'Le terme est formé à partir de deux lettres initiales de <strong>« twist »</strong> et les trois lettres finales de <strong>« jerk »</strong>, du nom des danses auxquelles il emprunte certains mouvements et déhanchés. Le <strong>twerk</strong> ou <strong>twerking</strong> est défini comme le fait de «danser de manière osé, secouer les hanches et les fesses.»');
INSERT INTO `discipline` (`idDiscipline`, `idProf`, `title`, `discipline`, `sousTitre`, `photo`, `alt`, `jour`, `heureDebut`, `heureFin`, `lieu`, `description`) VALUES(6, 1, 'House - Discipline - School Of Wild', 'House', 'Apprenez à trouver votre groove avec la house dance. Découvrez les bases et le feeling de légèreté de cette discipline.', 'prof-house.jpg', 'photo du prof de house', 'Mercredi', '14h30', '15h30', 'Mille et une étoiles Tsimbazaza', 'Tout comme le hip hop, la house music a inspiré sa propre danse, la <strong>danse house</strong>. Elle se traduit par un riche mélange de pas d''origine très diverses : Salsa, afro, claquettes ... Elle se reconnaît par un style léger et aérien.');
INSERT INTO `discipline` (`idDiscipline`, `idProf`, `title`, `discipline`, `sousTitre`, `photo`, `alt`, `jour`, `heureDebut`, `heureFin`, `lieu`, `description`) VALUES(7, 2, 'Dancehall - Discipline - School Of Wild', 'Dancehall', 'Apprenez les steps de base, le flow et l''attitude du dancehall. Old School, Middle School et New School, découvrez la vybz en mode Bad Man style ou Gyal style.', 'prof-dancehall.jpg', 'photo du prof de dancehall', 'Samedi', '14h30', '15h30', 'Mille et une étoiles Tsimbazaza', 'Né en Jamaïque ou plus précisément à Kingston au tout début des années 1980, le <strong>dancehall</strong> est apparu durant l''époque des changement sociaux et politiques et s''est rapidement propagé dans les Antilles avant d''atteindre le reste des pays francophones.');
INSERT INTO `discipline` (`idDiscipline`, `idProf`, `title`, `discipline`, `sousTitre`, `photo`, `alt`, `jour`, `heureDebut`, `heureFin`, `lieu`, `description`) VALUES(8, 3, 'Afro Fusion - Discipline - School Of Wild', 'Afro fusion', 'Alliez joie de vivre, énergie et danse. Découvrez les differentes bases de l''afro fusion, la culture et le feeling de la discipline.', 'prof-afro-fusion.jpg', 'photo du prof d''afro fusion', 'Samedi', '13h30', '14h30', 'Mille et une étoiles Tsimbazaza', '');

-- --------------------------------------------------------

--
-- Structure de la table `evenement`
--

CREATE TABLE IF NOT EXISTS `evenement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(20) DEFAULT NULL,
  `sousTitre` varchar(100) DEFAULT NULL,
  `dateEvenement` date DEFAULT NULL,
  `photo` varchar(400) DEFAULT NULL,
  `alt` varchar(50) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `evenement`
--

INSERT INTO `evenement` (`id`, `titre`, `sousTitre`, `dateEvenement`, `photo`, `alt`, `description`) VALUES(1, 'Let''s Dance', 'Spéctacle de fin d''année', '2018-07-03', 'affiche-lets-dance.jpg', 'affiche de l''événement Let''s Dance', 'LET’S DANCE a été le premier événement organisé par l’école de danse urbaine School of wild. Elle a pour but de présenter le travail des élèves  à l’aide du grand spectacle de fin de saison mais aussi d’offrir l’opportunité à de jeunes groupes de danseurs urbains de partager leur art , de se surpasser grâce aux différents concours de danse.  Il est rapidement devenu un événement incontournable dans le milieu de la danse et à sa cinquième édition, qui s’est déroulé en 2017, il est devenu un événe');
INSERT INTO `evenement` (`id`, `titre`, `sousTitre`, `dateEvenement`, `photo`, `alt`, `description`) VALUES(2, 'High Dih', 'Presentation officielle du groupe G', '2018-12-24', 'affiche-high-dih.jpg', 'affiche de l''événement High Dih', 'HIGH DIH est un événement de danse qui a pour but de rassembler les chorégraphes et les différents groupes de danse à Antananarivo. Il se divise en deux(2) parties : \r\n1.	Des workshops de chaque chorégraphe d’un groupe\r\n2.	Spectacle de danse\r\nPour les workshops, chaque chorégraphe représentant son groupe a une heure pour partager son art, sa discipline de danse et sa particularité.\r\nPour le grand spectacle, les groupes feront des shows pour le grand public, passionné de la danse.\r\nLa première éd');
INSERT INTO `evenement` (`id`, `titre`, `sousTitre`, `dateEvenement`, `photo`, `alt`, `description`) VALUES(3, 'We Are Wild', 'Presentation officielle du groupe We Are Wild', '2018-04-22', 'affiche-WAW.jpg', 'affiche de l''événement du groupe WAW', 'Pour élargir son contact avec l’extérieur, l’école de danse organisera un événement culturel qui visera à appliquer ce que les élèves ont appris depuis la rentrée. Au programme, on a choisi les six (6) meilleurs élèves pour donner un atelier de danse à un public de tout âge sous forme de battle (nouveau concept), sans aucun professeur suivi de prestations de tous les élèves. Afin de travailler le bon relationnel professeurs-élèves-public, il y aura différents concours de tout genre, animations e');
INSERT INTO `evenement` (`id`, `titre`, `sousTitre`, `dateEvenement`, `photo`, `alt`, `description`) VALUES(4, 'Night By G', 'Soirée présentation officielle du groupe G (part 2)', '2018-04-28', 'affiche-night-by-G.jpg', 'affiche de l''événement Night By G', 'G ("Ji") est un groupe de danse qui a été créé en Août 2017, composé de quatre (6) filles , formé pour mieux faire connaître les disciplines suivantes au grand public : Street Jazz, Vogue, Twerk et Waacking. Dans le style sensuel et glamour que dégage ces disciplines , il se démarque par une touche particulière et propre qui est de danser en chaussettes longues. Après une première soirée pour la présentation officielle du groupe au Kudeta  Carlton, il continue sa route pour une deuxième partie d');

-- --------------------------------------------------------

--
-- Structure de la table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomMenu` varchar(20) DEFAULT NULL,
  `url` varchar(20) DEFAULT NULL,
  `page` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `menu`
--

INSERT INTO `menu` (`id`, `nomMenu`, `url`, `page`) VALUES(1, 'Accueil', 'accueil-school-of-wi', 'index.php');
INSERT INTO `menu` (`id`, `nomMenu`, `url`, `page`) VALUES(2, 'A propos', 'a-propos-school-of-w', 'apropos');
INSERT INTO `menu` (`id`, `nomMenu`, `url`, `page`) VALUES(3, 'Cours', 'cours-school-of-wild', 'cours.php');
INSERT INTO `menu` (`id`, `nomMenu`, `url`, `page`) VALUES(4, 'Evénement', 'evenement-school-of-', 'evenement.php');
INSERT INTO `menu` (`id`, `nomMenu`, `url`, `page`) VALUES(5, 'Contact', 'contact-school-of-wi', 'contact.php');

-- --------------------------------------------------------

--
-- Structure de la table `presentation`
--

CREATE TABLE IF NOT EXISTS `presentation` (
  `contenu` varchar(800) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `presentation`
--

INSERT INTO `presentation` (`contenu`) VALUES('School of wild, crée en 2011, est la première école de danse purement urbaine à Madagascar. Mené par Andriamahery Frédérico du groupe GB Wild, elle ne se limite pas seulement à l''apprentissage de la danse avec des cours collectifs ou particuliers mais à un partage total dans le but de faire connaître un peu plus la danse au grand public grâce à differents concept d''événements. Depuis 2013 elle a réussi à se placer en tant que pionnier dans ce milieu avec son premier événement « Let''s Dance » qui s''est vu devenir événement national puis international depuis 2017; avec la collaboration de « Dancehall Master World » ; « High Dih » qui a concrétisé sa 2ème édition en fin décembre 2017 et d''autres à venir...');

-- --------------------------------------------------------

--
-- Structure de la table `prof`
--

CREATE TABLE IF NOT EXISTS `prof` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prenom` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `prof`
--

INSERT INTO `prof` (`id`, `prenom`) VALUES(1, 'Teetoon');
INSERT INTO `prof` (`id`, `prenom`) VALUES(2, 'Brenda');
INSERT INTO `prof` (`id`, `prenom`) VALUES(3, 'Hanitra');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
