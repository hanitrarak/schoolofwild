<?php 
	require('fonctions.php');
	$aLaUnes = get_AlaUne();
	$accueil = get_Accueil();
	$title = $accueil[0]['title'];
	$h1 = $accueil[0]['h1'];
	$h2 = $accueil[0]['h2'];
	$h3 = $accueil[0]['h3'];
	$url = $accueil[0]['url'];
	
	if (strtolower($_GET['url']) != $url ) {
		header('location:'.$url.'.html');
	}
?>

<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Description" content="School Of Wild une école de danse urbaine formée par les membres de GB Wild avec un feeling non stop">
    <title><?php echo $title ?></title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">      
	<link href="css/main.css" rel="stylesheet">
	 <link href="css/responsive.css" rel="stylesheet">
	 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]--> 
	
  </head>
  <body class="homepage">   
	<?php include('header.php'); ?>
	
	<div class="slider" style="margin-top: 88px">
			<div id="about-slider">
				<div id="carousel-slider" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
				  	<ol class="carousel-indicators visible-xs">
					    <li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
					    <li data-target="#carousel-slider" data-slide-to="1"></li>
					    <li data-target="#carousel-slider" data-slide-to="2"></li>
				  	</ol>

					<div class="carousel-inner">
					   <div class="item active" style="background: #000 url(images/tournage-noel-2016.jpg); background-size:cover; height:430px;">
							<!--<img src="images/lets-dance-1.jpg" class="img-responsive" alt="">-->
							</br></br></br></br></br></br></br></br></br></br></br></br></br>
							<h1 style="font-size: 45px; margin-left:50px;"><?php echo $h1 ?></h1>
					   </div>
					   <div class="item">
							<img src="images/lets-dance-5.jpg" class="img-responsive" alt="spectacle let's dance 5" title="tournage d'une video noel 2017">
					   </div> 
					   <div class="item">
							<img src="images/tournage-noel-2017.jpg" class="img-responsive" alt="tournage d'une video noel 2017" title="tournage d'une video noel 2017">
					   </div> 
					</div>
					
					<a class="left carousel-control hidden-xs" style="margin-left:50px" href="#carousel-slider" data-slide="prev">
						<i class="fa fa-angle-left"></i> 
					</a>
					
					<a class=" right carousel-control hidden-xs" style="margin-right:50px" href="#carousel-slider" data-slide="next">
						<i class="fa fa-angle-right"></i> 
					</a>
				</div> <!--/#carousel-slider-->
			</div><!--/#about-slider-->
		
	</div>

	 <section id="feature" style="margin-top:-25px" >
        <div class="container">
           <div class="center wow fadeInDown">
                <h2><?php echo $h2 ?></h2>
                <h3 class="lead"><?php echo $h3 ?></h3>
            </div>

            <div class="row" style="margin-top:-30px">
                <div class="features">
					<?php foreach($aLaUnes as $aLaUne) { ?>
						<div class="col-md-5 col-sm-6 wow fadeInDown" style="margin-left:60px; background-color: #dda505;" data-wow-duration="1000ms" data-wow-delay="600ms">
							<div class="feature-wrap" style="margin-top:15px;background-color: white;border-radius:15px">
								<img src="images/evenement/<?php echo $aLaUne['photo'] ?>" class="img-responsive" alt="<?php echo $aLaUne['alt'] ?>" title="<?php echo $aLaUne['alt'] ?>">
								<h2 align="center"><?php echo $aLaUne['titre']; ?></h2>
								<h3 align="center"><?php echo $aLaUne['sousTitre']; ?></h3>
								<!--<h4><?php echo $aLaUne['description']; ?></h4>-->
								<p align="center"><strong>Date :</strong> <?php echo $aLaUne['dateEvenement']; ?></p>
								<p align="center"><strong>Lieu :</strong> <?php echo $aLaUne['lieu']; ?></p>
							</div>
						</div><!--/.col-md-6-->
					<?php } ?>
                </div><!--/.services-->
            </div><!--/.row-->    
        </div><!--/.container-->
    </section><!--/#feature-->
	
	<?php include('footer.php'); ?>
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>   
    <script src="js/wow.min.js"></script>
	<script src="js/main.js"></script>
  </body>
</html>