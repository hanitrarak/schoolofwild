<?php 
	require('fonctions.php');
	$apropos = get_Apropos();
	$title = $apropos[0]['title'];
	$h1 = $apropos[0]['h1'];
	$h21 = $apropos[0]['h21'];
	$h31 = $apropos[0]['h31'];
	$h22 = $apropos[0]['h22'];
	$h32 = $apropos[0]['h32'];
	$url = $apropos[0]['url'];
	
	if (strtolower($_GET['url']) != $url ) {
		header('location:'.$url.'.html');
	}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="Description" content="School of wild, crée en 2011, est la première école de danse purement urbaine à Madagascar. Mené par Andriamahery Frédérico du groupe GB Wild, elle ne se limite pas seulement à l'apprentissage de la danse avec des cours collectifs ou particuliers mais à un partage total dans le but de faire connaître un peu plus la danse au grand public grâce à differents concept d'événements.">
    <title><?php echo $title ?></title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">      
	<link href="css/main.css" rel="stylesheet">
	 <link href="css/responsive.css" rel="stylesheet">
	 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
	
  </head>
  <body class="homepage">   
	<?php include('header.php'); ?>
	
	<section id="services" class="service-item">
	   <div class="container">
            <div class="center wow fadeInDown" style="margin-top:-20px; margin-left:90px; margin-right:90px">
                <h1 style="font-size:40px"><?php echo $h1 ?></h1></br>
                <h2><?php echo $h21 ?></h2>
                <h3 class="lead"><?php echo $h31 ?></h3>
				<h2 style="margin-top:30px"><?php echo $h22 ?></h2>
			    <h3 class="lead" style="margin-left:90px; margin-right:90px"><?php echo $h32 ?></h3>
            </div>
        </div><!--/.container-->
    </section><!--/#services-->

	<?php include('footer.php') ?>
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>   
    <script src="js/wow.min.js"></script>
	<script src="js/main.js"></script>
  </body>
</html>